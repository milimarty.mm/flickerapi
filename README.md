# FlickerApp

### used technologies in the project:
> Navigation Component 
> Dagger Hilt
> Paging 3
> Glide
> Retrofit
> Gson
> Coroutines
> ViewBinding
> LiveData
> DiffUtils
>
> BuildConfig for apiKey
### used architecture in the project:
> MVVM
```English 
	the project has two fragments and one activity.
	in other words its a single activity app.
	here is the overall structure of it
	you can see the packages in the root of the project:
```
* api: contains the classes which are for communicating with the server.
* areas: contains fragments along with models and view models
* di: contains classes related to dependency injection 
* gcomponent: contains custom views
* utils: contains utility classes used in the project
* FlickerApp
* MainActivity
![alt text](1.png "Title")

## Api Package 
### this package contains 
> repo
>> FlickerRepository :
>>> this repository class contains methods that get data from the server and transfer them >>> to the viewmodel using livedata objects.
>>> to inject the FlickerApi , hilt is used which contributes to paste of the development and >>> readability of the code.
>>> 

> AuthApi
> > its an interface containing the auth methods
>
> FlickerApi
>
> > its an interface that contains getPhoto and searchPhoto which are responsible for searching and getting the photos
>
> Models 
> this class contains auth models
>
> Response 
>
> > this is a generic class for getting the status and the data of the response
> > 
![alt text](2.png "Title")

# Areas Package 
>gallery
>> this package contains :
>>> PhotoLoadingAdapter:
>>>> this class is used in the adapter to handle the loading and retrying to get the data from the Rest.
when loading , it shows a progress bar and when catching an error, it shows the retry option

>>> GalleryItemModel 
>>>> this kotlin file contains models used in Recyclerview  and GalleryFragment
>>>
>>> GalleryPaginigSource
>>>> this class is responsible for loading new items when scrolling the recyclerview up and down.
it loads the new page when needed

>>> GalleryViewModel
>>>
>>> > this class is the ViewModel for GalleryFragment and contains performs storing and receiving data operations
>>> > 

>>> GalleryFragment 
>>> > GalleryFragment class contains every ui related operation.
for simpler access to ui elements and increasing the development paste and also cleaner code , ViewBinding is used.
the data taken from ui elements are sent to the ViewModel.
this architecture solves lots of problems such as keeping the state of Recyclerview when configuration changes happen.
this class is also responsible for getting the text from search bar and loading the data using the adapter.

![alt text](3.png "Title")



# PhotoDetails Package 

> PhotoDetailsModel 
>> this kotlin file contains models used in  PhotoDetailsFragment 
>> 

> PhotoDetailsViewModel
>> this class is the ViewModel for PhotoDetailsFragment  and contains performs storing and receiving data operations
>>

> PhotoDetailsFragment 
>> Just like GalleryFragment class this class is responsible for handling the data in UI elements 
There is variable in this class named args that contains image model which is sent from GalleryFragment to this view using Navigation Component and than according to id of the 
Required image data is loaded from repository into the viewmodel than into the UI  

![alt text](4.png "Title")

# Di Package 

This package contains a class named AppModule  which is singleton so it exist entire lifetime of the application.

# GComponent Package
this package contains CustomTextView , CustomEditText and Custom Button.
every one of these classes has a custom font and is alernated and customized for something.


# FlickerApp
attr is used to have both dark and light themes in the app but unfortunately due to lack of time , it is not implemented completely!

after entering the app , it starts requesting for data from the server. if there is no available internet connection or if there is a problem in the Rest , retry button appears.
after searching something , the relevant data is loaded in the same fragment.
you can scroll and get the rest of the images.
clicking on each image you can see its information.
clicking on the icon next to the search bar, you can change the number of items per row in the grid.



# UnitTest 

This section contains tokenAuth and basicAuth methods so you can test authentication .

``` kotlin
 @Test
    fun basicAuth() = runBlocking {

        val url = "https//example.com"
        val retrofit = Retrofit.Builder().baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val autApi: AuthApi = retrofit.create(AuthApi::class.java)

        val userName = "USERNAME"
        val password = "PASSWORD"

        val base = "$userName:$password"
        val authHeader = "Basic ${Base64.encode(base.toByteArray(), Base64.NO_WRAP)}"


        val job = CoroutineScope(IO).launch {
            val response = autApi.basicTokenAuth(authHeader)
            print(response.toString())
        }

        job.join()

    }


    @Test
    fun tokenAuth() = runBlocking {

        val url = "https//example.com"
        val retrofit = Retrofit.Builder().baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val autApi: AuthApi = retrofit.create(AuthApi::class.java)


        val login = Login("USERNAME", "PASSWORD")
        var response: User? = null
        val job = CoroutineScope(IO).launch {
            response = autApi.login(login)
            print(response.toString())
        }
        job.join()

        val job1 = CoroutineScope(IO).launch {
            val response1 = autApi.getSecret(response!!.token)
            print(response1.toString())
        }

        job1.join()

    }
```





