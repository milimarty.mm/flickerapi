package com.milimarty.flicker

import android.app.Application
import com.milimarty.flicker.utils.Constants
import com.milimarty.flicker.utils.Utils
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FlickerApp: Application() {
    override fun onCreate() {
        super.onCreate()
        /**
         * init GLOBAL CONTEXT
         */
        Constants._contextG = applicationContext

        Constants.phoneWidth = Utils.getPhoneWidth()

    }
}