package com.milimarty.flicker.di

import com.google.gson.GsonBuilder
import com.milimarty.flicker.api.FlickerApi
import com.milimarty.flicker.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        val gson = GsonBuilder()
            .setLenient()
            .create()
       return  Retrofit.Builder().baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        }
       

    @Provides
    @Singleton
    fun provideFlickerApi(retrofit: Retrofit):FlickerApi =
        retrofit.create(FlickerApi::class.java)

}