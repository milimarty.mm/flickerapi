package com.milimarty.flicker.api.repo


import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.milimarty.flicker.api.FlickerApi
import com.milimarty.flicker.areas.gallery.repo.GalleryPagingSource
import com.milimarty.flicker.utils.Constants.PARE_PAGE

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FlickerRepository @Inject constructor(private val flickerApi: FlickerApi) {


    fun searchPhoto(searchText: String) =
        Pager(
            config = PagingConfig(
                pageSize = PARE_PAGE,
                maxSize = 80,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                GalleryPagingSource(flickerApi, searchText)
            }
        ).liveData

    suspend fun getPhoto(id: String) = flickerApi.getPhoto(id = id)

}