package com.milimarty.flicker.api

import com.milimarty.flicker.BuildConfig
import com.milimarty.flicker.areas.gallery.model.GalleryItemModels
import com.milimarty.flicker.areas.photodetails.PhotoDetailsFragment
import com.milimarty.flicker.areas.photodetails.model.PhotoDetailsModel
import com.milimarty.flicker.utils.Constants.PARE_PAGE
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface FlickerApi {
    companion object {
        /**
         * ENUM FOR METHODS
         */
        enum class Methods(val method: String) {
            SEARCH_PHOTOS("flickr.photos.search"),
            GET_PHOTO("flickr.photos.getInfo")
        }

        const val FLICKER_API_KEY: String = BuildConfig.FILICKER_API_KEY

    }


    @GET("rest/")
    suspend fun searchPhotos(
        @Query("method") method: String = Methods.SEARCH_PHOTOS.method,
        @Query("page") page: Int,
        @Query("text") searchText: String,
        @Query("api_key") apiKey: String = FLICKER_API_KEY,
        @Query("format") responseFormat: String = "json",
        @Query("per_page") perPage: Int = PARE_PAGE,
        @Query("nojsoncallback") noJsonCallback: Int = 1,
    ): GalleryItemModels


    @GET("rest/")
    suspend fun getPhoto(
        @Query("method") method: String = Methods.GET_PHOTO.method,
        @Query("photo_id") id: String,
        @Query("api_key") apiKey: String = FLICKER_API_KEY,
        @Query("format") responseFormat: String = "json",
        @Query("nojsoncallback") noJsonCallback: Int = 1,
    ): PhotoDetailsModel









}