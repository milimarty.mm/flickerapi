package com.milimarty.flicker.api

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}