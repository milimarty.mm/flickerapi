package com.milimarty.flicker.api

import com.milimarty.flicker.areas.photodetails.model.PhotoDetailsModel
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header

interface AuthApi {


    @GET("/")
    suspend fun basicTokenAuth(
        @Header("Authorization") authHeader:String
    )

    @GET("/")
    suspend fun login(
        @Body login:Login
    ):User

    @GET("/")
    suspend fun getSecret(
        @Header("Authorization") authHeader:String
    )


}