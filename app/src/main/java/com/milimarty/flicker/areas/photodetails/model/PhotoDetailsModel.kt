package com.milimarty.flicker.areas.photodetails.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.milimarty.flicker.areas.gallery.model.GalleryItemModels
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhotoDetailsModel(
    @SerializedName("photo")
    val item:PhotoItem,
    @SerializedName("stat")
    val status: String
) : Parcelable{
    @Parcelize
    data class PhotoItem(
        @SerializedName("title")
        val title: Content,
        @SerializedName("description")
        val description: Content,
        @SerializedName("comments")
        val comments: Content,
    ) : Parcelable

}

@Parcelize
data class Content(
    @SerializedName("_content")
    val content: String
) : Parcelable