package com.milimarty.flicker.areas.gallery.adapter

import android.R.attr.*
import android.annotation.SuppressLint
import android.app.ActionBar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.milimarty.flicker.R
import com.milimarty.flicker.areas.gallery.model.GalleryItemModels
import com.milimarty.flicker.databinding.PhotoItemCardViewBinding
import com.milimarty.flicker.utils.Constants
import com.milimarty.flicker.utils.Utils


class GalleryAdapter(private val  onItemClickListener:(GalleryItemModels.Photo)->Unit) :
    PagingDataAdapter<GalleryItemModels.Photo, GalleryAdapter.PhotoItemViewHolder>(diffUtils) {

    var gridCount = 1

    companion object {
        private val diffUtils = object : DiffUtil.ItemCallback<GalleryItemModels.Photo>() {
            override fun areItemsTheSame(
                oldItem: GalleryItemModels.Photo,
                newItem: GalleryItemModels.Photo
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: GalleryItemModels.Photo,
                newItem: GalleryItemModels.Photo
            ): Boolean {
                return oldItem == newItem
            }


        }
    }

    override fun onBindViewHolder(holder: PhotoItemViewHolder, position: Int) {
        val item = getItem(position)
        item?.let {
            val dimen = Constants.phoneWidth / gridCount
            val params = LinearLayout.LayoutParams(
                dimen,
                dimen
            )
            if (gridCount > 1)
            {
                val margin = Utils.dpToPx(4f).toInt()
                params.setMargins(margin, margin, margin, margin)

            }
            holder.itemView.layoutParams = params

            holder.bind(it)
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoItemViewHolder {
        val binding =
            PhotoItemCardViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PhotoItemViewHolder(binding)

    }


    inner class PhotoItemViewHolder(private val binding: PhotoItemCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                val position =  bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    item?.let {
                        onItemClickListener(it)
                    }
                }
            }

        }

        @SuppressLint("CheckResult")
        fun bind(photoItem: GalleryItemModels.Photo) {
            binding.photoItem = photoItem

            /**
             *  Glide for load image from url to image view
             *  @param binding.photoItemImageView
             */
            Glide
                .with(binding.photoItemImageView)
                .load(photoItem.photoUrl)
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .error(R.drawable.flickr_icon)
                .into(binding.photoItemImageView)
        }

    }


}