package com.milimarty.flicker.areas.photodetails.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.milimarty.flicker.api.Response
import com.milimarty.flicker.api.Status
import com.milimarty.flicker.api.repo.FlickerRepository
import com.milimarty.flicker.areas.photodetails.model.PhotoDetailsModel
import kotlinx.coroutines.launch
import java.lang.Exception

class PhotoDetailsViewModel @ViewModelInject constructor(private val repository: FlickerRepository) :
    ViewModel() {

    private val _res = MutableLiveData<Response<PhotoDetailsModel>>()

    val res: LiveData<Response<PhotoDetailsModel>>
        get() = _res


    fun getPhoto(photoId: String) = viewModelScope.launch {
        _res.postValue(Response.loading(null))
        var res: Response<PhotoDetailsModel>
        try {
            val item = repository.getPhoto(id = photoId)
            res = Response(Status.SUCCESS, data = item)
            _res.postValue(res)
        } catch (e: Exception) {
            res = Response(Status.ERROR, message = e.message)
            _res.postValue(res)
        }


    }

}