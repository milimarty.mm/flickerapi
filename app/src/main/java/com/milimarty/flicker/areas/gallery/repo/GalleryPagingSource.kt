package com.milimarty.flicker.areas.gallery.repo

import android.util.Log
import androidx.paging.PagingSource
import com.milimarty.flicker.api.FlickerApi
import com.milimarty.flicker.areas.gallery.model.GalleryItemModels
import com.milimarty.flicker.utils.Constants
import java.lang.Exception

class GalleryPagingSource(
    private val flickerApi: FlickerApi,
    private val searchText: String
) : PagingSource<Int, GalleryItemModels.Photo>() {


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, GalleryItemModels.Photo> {


        return try {
            val position = params.key ?: Constants.FIRST_PAGE_INDEX
            val response = flickerApi.searchPhotos(
                searchText = searchText,
                page = position,
                perPage = params.loadSize
            )
            Log.d("RESPONSE-REQUEST", response.toString())

           return LoadResult.Page(
                data = response.item.photo ,
                prevKey = if (position == Constants.FIRST_PAGE_INDEX) null else position - 1,
                nextKey = if (response.item.photo.isEmpty()) null else position + 1

            )

        } catch (e: Exception) {
            e.printStackTrace()
            return  LoadResult.Error(e)
        }


    }
}