package com.milimarty.flicker.areas.gallery.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import androidx.paging.cachedIn
import com.milimarty.flicker.api.Response
import com.milimarty.flicker.api.repo.FlickerRepository
import com.milimarty.flicker.areas.photodetails.model.PhotoDetailsModel
import com.milimarty.flicker.utils.Constants

class GalleryViewModel @ViewModelInject constructor(private val repository: FlickerRepository) :
    ViewModel() {

    private val searchTextLiveData = MutableLiveData(Constants.DEFAULT_SEARCH_TEXT)
    val photoList = searchTextLiveData.switchMap {
        repository.searchPhoto(it).cachedIn(viewModelScope)
    }

    fun searchPhotos(searchText: String) {
        searchTextLiveData.value = searchText
    }

//
//    private val _listModel = MutableLiveData<Int>(0)
//    val listModel: LiveData<Int>
//        get() = _listModel
//
//    fun setListModel(model: Int) {
//        _listModel.postValue(model)
//    }


}