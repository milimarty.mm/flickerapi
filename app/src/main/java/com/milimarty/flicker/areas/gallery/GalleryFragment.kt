package com.milimarty.flicker.areas.gallery

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.milimarty.flicker.R
import com.milimarty.flicker.areas.gallery.adapter.GalleryAdapter
import com.milimarty.flicker.areas.gallery.adapter.PhotoLoadingAdapter
import com.milimarty.flicker.areas.gallery.viewmodel.GalleryViewModel
import com.milimarty.flicker.databinding.FragmentGalleryBinding
import com.milimarty.flicker.utils.Utils.hideSoftKeyboard
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GalleryFragment : Fragment(R.layout.fragment_gallery) {
    /**
     * Values
     */
    private val viewModel by viewModels<GalleryViewModel>()

    private var _mBinding: FragmentGalleryBinding? = null
    private val mBinding
        get() = _mBinding!!

    private lateinit var galleryAdapter: GalleryAdapter


    private var gridCount = 1
    private var listModel = 0


    private fun init() {
        /**
         * init Gallery Adapter
         */
        galleryAdapter = GalleryAdapter {
            val action = GalleryFragmentDirections.actionGalleryFragmentToPhotoDetailsFragment(it)
            findNavController().navigate(action)
        }

        setupRecyclerView()
        /**
         * init search box
         */
        initSearchBox()

        /**
         * Loading View
         */
        initLoadState()

    }

    /**
     * init loading state
     */
    private fun initLoadState() {

        galleryAdapter.addLoadStateListener { it ->
            mBinding.apply {
                fragmentGalleryRecyclerViewLoading.isVisible =
                    it.source.refresh is LoadState.Loading
                fragmentGalleryRecyclerView.isVisible = it.source.refresh is LoadState.NotLoading
                fragmentGalleryErrorTextView.isVisible = it.source.refresh is LoadState.Error
                fragmentGalleryRetryButton.isVisible = it.source.refresh is LoadState.Error

                if (it.source.refresh is LoadState.NotLoading && it.append.endOfPaginationReached && galleryAdapter.itemCount < 1) {
                    fragmentGalleryRecyclerView.isVisible = false
                    fragmentGalleryEmptyTextView.isVisible = true
                } else {
                    fragmentGalleryEmptyTextView.isVisible = false

                }

            }
        }
    }

    /**
     *init search box
     */
    private fun initSearchBox() {
        mBinding.fragmentGallerySearchEditText.apply {
            imeOptions = EditorInfo.IME_ACTION_SEARCH
            setOnEditorActionListener(OnEditorActionListener { v, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (v.text.toString().isNotEmpty() || v.text.toString() != " ") {
                        clearFocus()
                        mBinding.fragmentGalleryRecyclerView.scrollToPosition(0)
                        viewModel.searchPhotos(v.text.toString())
                        hideSoftKeyboard(mBinding.fragmentGallerySearchEditText)
                        return@OnEditorActionListener true
                    }
                    Toast.makeText(context, "please insert correct text .", Toast.LENGTH_SHORT)
                        .show()

                    return@OnEditorActionListener false
                }
                false
            })

        }

    }

    private fun setupRecyclerView() {


        mBinding.fragmentGalleryRecyclerView.apply {


            hasFixedSize()
            adapter = galleryAdapter.withLoadStateHeaderAndFooter(
                header = PhotoLoadingAdapter { galleryAdapter.retry() },
                footer = PhotoLoadingAdapter { galleryAdapter.retry() }
            )
                when (listModel) {
                    0 -> {
                        layoutManager = LinearLayoutManager(context)
                        galleryAdapter.gridCount = gridCount
                    }
                    1, 2 -> {
                        layoutManager = GridLayoutManager(context, gridCount)
                        galleryAdapter.gridCount = gridCount
                    }
                }

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _mBinding = FragmentGalleryBinding.bind(view)
        init()

        viewModel.photoList.observe(viewLifecycleOwner) {
            Log.d("GALLERY_FRAGMENT", it.toString())
            galleryAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        }



        mBinding.fragmentGalleryGirdImageBtn.setOnClickListener {

            when (listModel) {
                0 -> {
                    it.setBackgroundResource(R.drawable.grid_view_2_icon)
                    listModel = 1
                    gridCount = 2
                }
                1 -> {
                    it.setBackgroundResource(R.drawable.list_view_icon)
                    listModel = 2
                    gridCount = 3
                }
                2 -> {
                    it.setBackgroundResource(R.drawable.grid_view_icon)
                    listModel = 0
                    gridCount = 1
                }
            }
            setupRecyclerView()

        }


    }



    override fun onDestroy() {
        super.onDestroy()
        _mBinding = null
    }
}