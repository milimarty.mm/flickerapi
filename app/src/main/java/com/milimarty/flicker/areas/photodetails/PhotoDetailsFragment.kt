package com.milimarty.flicker.areas.photodetails

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.milimarty.flicker.R
import com.milimarty.flicker.api.Status.*
import com.milimarty.flicker.areas.photodetails.model.PhotoDetailsModel
import com.milimarty.flicker.areas.photodetails.viewmodel.PhotoDetailsViewModel
import com.milimarty.flicker.databinding.FragmentPhotoDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PhotoDetailsFragment : Fragment(R.layout.fragment_photo_details) {

    private val viewModel by viewModels<PhotoDetailsViewModel>()
    private var _mBinding: FragmentPhotoDetailsBinding? = null
    private val mBinding
        get() = _mBinding!!

    /**
     * photoItem
     */
    private val args by navArgs<PhotoDetailsFragmentArgs>()

    /**
     * initToolbar
     */
    private fun initToolbar(){
        mBinding.fragmentPhotoToolbar.apply {
            setNavigationIcon(R.drawable.baseline_arrow_back_icon)
            setNavigationOnClickListener { findNavController().navigateUp() }
        }
    }

    /**
     * loadData
     */
    private fun loadData() {

        viewModel.getPhoto(args.photoItem.id)
        viewModel.res.observe(viewLifecycleOwner) {
            when (it.status) {
                SUCCESS -> {
                    val data = it.data!!.item as PhotoDetailsModel.PhotoItem
                    mBinding.apply {
                        fragmentPhotoLoading.isVisible = false
                        fragmentPhotoTitleTextView.text = data.title.content
                        fragmentPhotoDescriptionTextView.text = data.description.content
                        fragmentPhotoCommentTextView.text = data.comments.content

                        Glide
                            .with(mBinding.fragmentPhotoImageView)
                            .load(args.photoItem.photoUrl)
                            .centerCrop()
                            .transition(DrawableTransitionOptions.withCrossFade())
                            .error(R.drawable.flickr_icon)
                            .into(mBinding.fragmentPhotoImageView)
                    }
                }
                ERROR -> {


                }
                LOADING -> {

                    mBinding.apply {
                        fragmentPhotoLoading.isVisible = true
                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _mBinding = FragmentPhotoDetailsBinding.bind(view)

        initToolbar()
        loadData()

    }

    override fun onDestroy() {
        super.onDestroy()
        _mBinding = null
    }
}