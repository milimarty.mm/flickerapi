package com.milimarty.flicker.areas.gallery.model

import android.icu.text.CaseMap
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class GalleryItemModels(
    @SerializedName("photos")
    val item: Photos,
    @SerializedName("stat")
    val status: String
) : Parcelable {

    @Parcelize
    data class Photos(
        @SerializedName("page")
        val page: Int,
        @SerializedName("total")
        val total: String,
        @SerializedName("photo")
        val photo: @RawValue List<Photo>,
    ) : Parcelable

    @Parcelize
    data class Photo(
        @SerializedName("id")
        val id: String,
        @SerializedName("secret")
        val secret: String,
        @SerializedName("title")
        val title: String,
        @SerializedName("farm")
        val farmId: Int,
        @SerializedName("server")
        val serverId: Long
    ) : Parcelable {
        val photoUrl
            get() = "https://farm${farmId}.staticflickr.com/${serverId}/${id}_${secret}.jpg"
    }

}