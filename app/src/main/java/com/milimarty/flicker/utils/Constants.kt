package com.milimarty.flicker.utils

import android.content.Context

object Constants {
    /**
     * Fonts
     */
    var customFont:String = "font/sf_font.otf"

    /**
     * Global Context
     */
    lateinit var _contextG: Context

    /**
     * BASE URL
     */
    const val BASE_URL:String = "https://api.flickr.com/services/"

    var PARE_PAGE:Int = 5

    const val FIRST_PAGE_INDEX = 1

    const val DEFAULT_SEARCH_TEXT = "flicker"

    var phoneWidth:Int = 0


}