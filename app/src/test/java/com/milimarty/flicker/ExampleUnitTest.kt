package com.milimarty.flicker

import android.util.Base64
import android.util.Log
import com.milimarty.flicker.api.AuthApi
import com.milimarty.flicker.api.FlickerApi
import com.milimarty.flicker.api.Login
import com.milimarty.flicker.api.User
import com.milimarty.flicker.utils.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.Test

import org.junit.Assert.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {


    @Test
    fun basicAuth() = runBlocking {

        val url = "https//example.com"
        val retrofit = Retrofit.Builder().baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val autApi: AuthApi = retrofit.create(AuthApi::class.java)

        val userName = "USERNAME"
        val password = "PASSWORD"

        val base = "$userName:$password"
        val authHeader = "Basic ${Base64.encode(base.toByteArray(), Base64.NO_WRAP)}"


        val job = CoroutineScope(IO).launch {
            val response = autApi.basicTokenAuth(authHeader)
            print(response.toString())
        }

        job.join()

    }


    @Test
    fun tokenAuth() = runBlocking {

        val url = "https//example.com"
        val retrofit = Retrofit.Builder().baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val autApi: AuthApi = retrofit.create(AuthApi::class.java)


        val login = Login("USERNAME", "PASSWORD")
        var response: User? = null
        val job = CoroutineScope(IO).launch {
            response = autApi.login(login)
            print(response.toString())
        }
        job.join()

        val job1 = CoroutineScope(IO).launch {
            val response1 = autApi.getSecret(response!!.token)
            print(response1.toString())
        }

        job1.join()

    }
}